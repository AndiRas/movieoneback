Technologie : Symfony 4 
Pré-requis :  -php 7.2 minimum
              -composer
              -serveur apache + mysql
              
Installation :
-télécharger le codes source et le copier dans le répertoire de votre serveur
-importer la base de données et changer la ligne avec  DATABASE_URL dans le fichier .env du projet 
-utiliser la commande 'composer install' dans le répertoire du projet pour installer les dépendances
-créer un virtual host sur votre serveur comme celui-ci :

<VirtualHost *:80>
    ServerName domain.tld
    ServerAlias www.domain.tld

    DocumentRoot /var/www/project/public
    <Directory /var/www/project/public>
        AllowOverride All
        Order Allow,Deny
        Allow from All
    </Directory>

</VirtualHost>

-Activer le mod_rewrite de votre serveur apache
