<?php

namespace App\Controller;

use App\Entity\Film;
use App\Form\FilmType;
use App\Repository\FilmRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Image;
use App\Repository\GenreRepository;
use App\Repository\ImageRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/film")
 */
class FilmController extends AbstractController
{
    /**
     * @Route("/", name="film_index", methods={"GET"})
     */
    public function index(FilmRepository $filmRepository): Response
    {
        $films = $filmRepository->findAll();
        foreach($films as $f){
            $f->getImages();
        }
        return $this->render('film/index.html.twig', [
            'films' => $films
        ]);
    }

    /**
     * @Route("/new", name="film_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $film = new Film();
        $form = $this->createForm(FilmType::class, $film);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($film);
            $entityManager->flush();

            return $this->redirectToRoute('film_index');
        }

        return $this->render('film/new.html.twig', [
            'film' => $film,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="film_show", methods={"GET"})
     */
    public function show(Film $film): Response
    {
        $film->getImages();
        $film->getGenres();
        return $this->render('film/show.html.twig', [
            'film' => $film,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="film_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Film $film,GenreRepository $genreRepository): Response
    {
        $film->getImages();
        $film->getGenres();
        $genres = $genreRepository->findAll();
        $form = $this->createForm(FilmType::class, $film);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('film_index', [
                'id' => $film->getId(),
            ]);
        }

        return $this->render('film/edit.html.twig', [
            'film' => $film,
            'genres' => $genres,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="film_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Film $film): Response
    {
        if ($this->isCsrfTokenValid('delete'.$film->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($film);
            $entityManager->flush();
        }

        return $this->redirectToRoute('film_index');
    }

    /**
     * @Route("/add/image", name="add_image")
     */
    public function addImage(Request $request,FilmRepository $filmRepository,string $uploadDir,string $host): Response
    {
        $id = $request->get('id');
        $type = $request->get('type');
        $file = $request->files->get('image');
        if (empty($file)) 
        {
            return new Response("No file specified",  
               Response::HTTP_UNPROCESSABLE_ENTITY, ['content-type' => 'text/plain']);
        }        

        $filename = $file->getClientOriginalName();
        $file->move($uploadDir,$filename);
        $path = $host.'/assets/images/'.$filename;
        $film = $filmRepository->find($id);
        $image = new Image();
        $image->setFilm($film);
        $image->setPath($path);
        $image->setType($type);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($image);
        $entityManager->flush();
        return $this->redirectToRoute('film_show', ['id' => $id]);
    }
     /**
     * @Route("/delete/image/{idf}/{idi}", name="delete_film_image")
     */
    public function deleteFilmImage($idf,$idi,FilmRepository $filmRepository,ImageRepository $imageRepository){
        $entityManager = $this->getDoctrine()->getManager();
        $film = $filmRepository->find($idf);
        $image = $imageRepository->find($idi);
        $film->removeImage($image);
        $entityManager->remove($image);
        $entityManager->flush();
        return $this->redirectToRoute('film_edit', ['id' => $idf]);
    }
    /**
     * @Route("/delete/genre/{idf}/{idg}", name="delete_film_genre")
     */
    public function deleteFilmGenre($idf,$idg,FilmRepository $filmRepository,GenreRepository $genreRepository){
        $entityManager = $this->getDoctrine()->getManager();
        $film = $filmRepository->find($idf);
        $genre = $genreRepository->find($idg);
        $film->removeGenre($genre);
        $entityManager->flush();
        return $this->redirectToRoute('film_edit', ['id' => $idf]);
    }

     /**
     * @Route("/add/genre", name="add_film_genre")
     */
    public function addFilmGenre(Request $request,FilmRepository $filmRepository,GenreRepository $genreRepository){
        $idf = $request->get('idf');
        $idg = $request->get('idg');
        $entityManager = $this->getDoctrine()->getManager();
        $film = $filmRepository->find($idf);
        $genre = $genreRepository->find($idg);
        $film->addGenre($genre);
        $entityManager->flush();
        return $this->redirectToRoute('film_edit', ['id' => $idf]);
    }
}
